// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSProjectCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "TDSProject/Core/TDSProjectGameInstance.h"
#include "TDSProject/Weapons/WeaponDefault.h"

ATDSProjectCharacter::ATDSProjectCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Inventory Component
	InventoryComponent = CreateDefaultSubobject<UInventoryComponent>(TEXT("InventoryComponent"));
	if (InventoryComponent)
	{
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &ATDSProjectCharacter::InitWeapon);
	}

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATDSProjectCharacter::BeginPlay()
{
	Super::BeginPlay();

	//InitWeapon(InitWeaponName);

	if (CursorMaterial)
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
	}
}

void ATDSProjectCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	MovementTick(DeltaSeconds);

	if (CurrentCursor)
	{
		APlayerController* MyPC = Cast<APlayerController>(GetController());
		if (MyPC)
		{
			FHitResult TraceHitResult;
			MyPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}
}

void ATDSProjectCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	InputComponent->BindAxis(TEXT("MoveForward"), this, &ATDSProjectCharacter::InputAxisX);
	InputComponent->BindAxis(TEXT("MoveRight"), this, &ATDSProjectCharacter::InputAxisY);

	InputComponent->BindAction(TEXT("FireEvent"), IE_Pressed, this, &ATDSProjectCharacter::InputAttackPressed);
	InputComponent->BindAction(TEXT("FireEvent"), IE_Released, this, &ATDSProjectCharacter::InputAttackReleased);
	InputComponent->BindAction(TEXT("ReloadingEvent"), IE_Pressed, this, &ATDSProjectCharacter::TryReloadWeapon);
}

void ATDSProjectCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ATDSProjectCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void ATDSProjectCharacter::InputAttackPressed()
{
	AttackCharEvent(true);
}

void ATDSProjectCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

void ATDSProjectCharacter::MovementTick(float DeltaTime)
{
	AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
	AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);


	APlayerController* MyController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (MyController && !SprintEnable && !RollEnable)
	{
		// First way(Looks at any surface(Fixed by GameTraceChannel))
		FHitResult HitResult;
		MyController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, HitResult);
		float YawRotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), HitResult.Location).Yaw;
		SetActorRotation(FQuat(FRotator(0.0f, YawRotation, 0.0f)));

		// Second way(not working with camera rotation)
		// Get mouse position on screen
		/*float xMouse;
		float yMouse;
		MyController->GetMousePosition(xMouse, yMouse);
		// Get Character position on screen
		FVector CharLoc = GetActorLocation();
		FVector2D CharInScreen;
		MyController->ProjectWorldLocationToScreen(CharLoc, CharInScreen);
		// Get mouse position relative to the Character.
		FVector2D Result;
		Result.X = - (yMouse - CharInScreen.Y);
		Result.Y = xMouse - CharInScreen.X;
		// Get angle rotation and rotation Character
		float angle = FMath::RadiansToDegrees(FMath::Acos(Result.X/Result.Size()));

		if (Result.Y < 0)
			angle = 360 - angle;

		FRotator rot(0, angle, 0);
		SetActorRotation(rot);*/

		// Third way(Rotation only 180 degrees)
		/*FVector mouseLocation, mouseDirection;
		MyController->DeprojectMousePositionToWorld(mouseLocation, mouseDirection);
		FRotator targetRotation = mouseDirection.Rotation();
		FRotator currentCharacterRotation = GetActorRotation();
		FRotator newRotation = FRotator(currentCharacterRotation.Pitch, targetRotation.Yaw, currentCharacterRotation.Roll);
		SetActorRotation(newRotation);*/

		if (CurrentWeapon)
		{
			FVector Displacement = FVector(0);
			switch (MovementState)
			{
			case EMovementState::Aim_State:
				Displacement = FVector(0.0f, 0.0f, 160.0f);
				CurrentWeapon->bShouldReduceDispersion = true;
				break;
			case EMovementState::AimCrouch_State:
				CurrentWeapon->bShouldReduceDispersion = true;
				Displacement = FVector(0.0f, 0.0f, 120.0f);
				break;
			case EMovementState::Crouch_State:
				Displacement = FVector(0.0f, 0.0f, 80.0f);
				CurrentWeapon->bShouldReduceDispersion = false;
				break;
			case EMovementState::Run_State:
				Displacement = FVector(0.0f, 0.0f, 120.0f);
				CurrentWeapon->bShouldReduceDispersion = false;
				break;
			case EMovementState::Sprint_State:
				break;
			default:
				break;
			}

			CurrentWeapon->ShootEndLocation = HitResult.Location + Displacement;
			//aim cursor like 3d Widget?
		}
	}
}

void ATDSProjectCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		//ToDo Check melee or range
		myWeapon->SetWeaponStateFire(bIsFiring);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::AttackCharEvent - CurrentWeapon -NULL"));
	}
}

// Set movement speed from character movement state
void ATDSProjectCharacter::CharacterUpdate()
{
	float ResSpeed = 600.0f;
	switch (MovementState)
	{
	case EMovementState::Run_State:
		ResSpeed = MovementInfo.RunSpeed;
		break;
	case EMovementState::Aim_State:
		ResSpeed = MovementInfo.AimSpeed;
		break;
	case EMovementState::AimCrouch_State:
		ResSpeed = MovementInfo.AimCrouchSpeed;
		break;
	case EMovementState::Crouch_State:
		ResSpeed = MovementInfo.CrouchSpeed;
		break;
	case EMovementState::Sprint_State:
		ResSpeed = MovementInfo.SprintSpeed;
		break;
	default:
		break;
	}
	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

// Actual movement state
void ATDSProjectCharacter::ChangeMovementState()
{
	if (!AimEnable && !CrouchEnable && !SprintEnable)
	{
		MovementState = EMovementState::Run_State;
	}
	else
	{
		if (SprintEnable)
		{
			AimEnable = false;
			CrouchEnable = false;
			MovementState = EMovementState::Sprint_State;
		}
		else
		{
			if (AimEnable && CrouchEnable)
			{
				MovementState = EMovementState::AimCrouch_State;
			}
			else
			{
				if (AimEnable)
				{
					SprintEnable = false;
					MovementState = EMovementState::Aim_State;
				}
				if (CrouchEnable)
				{
					SprintEnable = false;
					MovementState = EMovementState::Crouch_State;
				}
			}
		}
	}
	CharacterUpdate();

	//Weapon state update
	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->UpdateStateWeapon(MovementState);
	}
}

AWeaponDefault* ATDSProjectCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void ATDSProjectCharacter::InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon)
{
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}
	
	UTDSProjectGameInstance* myGI = Cast<UTDSProjectGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (myGI)
	{
		if (myGI->GetWeaponInfoByName(IdWeaponName, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(
					GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
					CurrentWeapon = myWeapon;

					myWeapon->WeaponSettings = myWeaponInfo;
					myWeapon->ReloadTimer = myWeaponInfo.ReloadTime;
					//myWeapon->AdditionalWeaponInfo.Round = myWeaponInfo.MaxRound;
					myWeapon->UpdateStateWeapon(MovementState);

					myWeapon->AdditionalWeaponInfo = WeaponAdditionalInfo;
					//if(InventoryComponent)
					CurrentWeaponIndex = NewCurrentIndexWeapon;//fix

					myWeapon->OnWeaponFireStart.AddDynamic(this, &ATDSProjectCharacter::WeaponFireStart);

					myWeapon->OnWeaponReloadStart.AddDynamic(this, &ATDSProjectCharacter::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATDSProjectCharacter::WeaponReloadEnd);

					// after switch try reload weapon if needed
					if (CurrentWeapon->GetWeaponRound() <=0 && CurrentWeapon->CheckCanWeaponReload())
						CurrentWeapon->InitReload();

					if(InventoryComponent)
						InventoryComponent->OnWeaponAmmoAvailable.Broadcast(myWeapon->WeaponSettings.WeaponType);
				}
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::InitWeapon - Weapon not found in table -NULL"));
		}
	}
}

void ATDSProjectCharacter::RemoveCurrentWeapon()
{
}

void ATDSProjectCharacter::TryReloadWeapon()
{
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading)
	{
		if (CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSettings.MaxRound)
			CurrentWeapon->InitReload();
	}
}

void ATDSProjectCharacter::WeaponFireStart(UAnimMontage* AnimMontage)
{
	if(InventoryComponent && CurrentWeapon)
		InventoryComponent->SetAdditionalInfoWeapon(CurrentWeaponIndex, CurrentWeapon->AdditionalWeaponInfo);
	WeaponFireStart_BP(AnimMontage);
}

void ATDSProjectCharacter::WeaponReloadStart(UAnimMontage* AnimMontage)
{
	WeaponReloadStart_BP(AnimMontage);
}

void ATDSProjectCharacter::WeaponReloadEnd(bool bIsSuccess, int32 AmmoTake)
{
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->AmmoSlotChangeValue(CurrentWeapon->WeaponSettings.WeaponType, AmmoTake);
		InventoryComponent->SetAdditionalInfoWeapon(CurrentWeaponIndex, CurrentWeapon->AdditionalWeaponInfo);
	}
	WeaponReloadEnd_BP(bIsSuccess);
}

void ATDSProjectCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* AnimMontage)
{
	// in BP
}

void ATDSProjectCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess)
{
	// in BP
}

void ATDSProjectCharacter::WeaponFireStart_BP_Implementation(UAnimMontage* AnimMontage)
{
	// in BP
}

UDecalComponent* ATDSProjectCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}

void ATDSProjectCharacter::TrySwitchNextWeapon()
{
	if (InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more then one weapon go switch
		int8 OldIndex = CurrentWeaponIndex;
		if (CurrentWeapon)
		{
			FAdditionalWeaponInfo OldInfo;
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
			if (CurrentWeapon->WeaponReloading)
				CurrentWeapon->CancelReload();
		}
	}
}

void ATDSProjectCharacter::TrySwitchPreviousWeapon()
{
	if (InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more then one weapon go switch
		int8 OldIndex = CurrentWeaponIndex;
		if (CurrentWeapon)
		{
			FAdditionalWeaponInfo OldInfo;
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
			if (CurrentWeapon->WeaponReloading)
				CurrentWeapon->CancelReload();
		}
	}
}
