// Copyright Epic Games, Inc. All Rights Reserved.

// ReSharper disable CppUE4ProbableMemoryIssuesWithUObject
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TDSProject/FunctionLibrary/Types.h"
#include "TDSProject/Weapons/WeaponDefault.h"
#include "TDSProject/Character/InventoryComponent.h"
#include "TDSProjectCharacter.generated.h"

UCLASS(Blueprintable)
class ATDSProjectCharacter : public ACharacter
{
	GENERATED_BODY()
protected:
	virtual void BeginPlay() override;

public:
	ATDSProjectCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UInventoryComponent* InventoryComponent;

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

public:
	// Properties
	/*Movement*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	FCharacterSpeed MovementInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	bool AimEnable = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	bool CrouchEnable = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	bool SprintEnable = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	bool RollEnable = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	bool AimCrouchEnable = false;

	/*Cursor*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Cursor")
	UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Cursor")
	FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);
	UDecalComponent* CurrentCursor = nullptr;

	/*Weapon*/
	AWeaponDefault* CurrentWeapon = nullptr;

	/*Demo*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
	FName InitWeaponName;

	// Functions
	/*Inputs*/
	UFUNCTION()
	void InputAxisX(float Value);
	UFUNCTION()
	void InputAxisY(float Value);
	UFUNCTION()
	void InputAttackPressed();
	UFUNCTION()
	void InputAttackReleased();

	float AxisX = 0.0f;
	float AxisY = 0.0f;

	UFUNCTION(BlueprintCallable)
	void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
	void ChangeMovementState();
	UFUNCTION(BlueprintCallable)
	void AttackCharEvent(bool bIsFiring);

	UFUNCTION(BlueprintCallable)
	AWeaponDefault* GetCurrentWeapon();
	UFUNCTION(BlueprintCallable)
	void InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon);
	UFUNCTION(BlueprintCallable)//VisualOnly
	void RemoveCurrentWeapon();
	UFUNCTION(BlueprintCallable)
	void TryReloadWeapon();
	UFUNCTION()
	void WeaponFireStart(UAnimMontage* AnimMontage);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponFireStart_BP(UAnimMontage* AnimMontage);
	UFUNCTION()
	void WeaponReloadStart(UAnimMontage* AnimMontage);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadStart_BP(UAnimMontage* AnimMontage);
	UFUNCTION()
	void WeaponReloadEnd(bool bIsSuccess, int32 AmmoSafe);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadEnd_BP(bool bIsSuccess);

	UFUNCTION(BlueprintCallable)
	UDecalComponent* GetCursorToWorld();

	// Inventory
	void TrySwitchNextWeapon();
	void TrySwitchPreviousWeapon();

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	int32 CurrentWeaponIndex = 0;

	// Tick Function
	UFUNCTION()
	void MovementTick(float DeltaTime);
};

