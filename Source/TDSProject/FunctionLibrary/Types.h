// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"
#include "Types.generated.h"

UENUM(BlueprintType)
enum class EMovementState : uint8
{
	Aim_State UMETA(DisplayName = "Aim State"),
	Sprint_State UMETA(DisplayName = "Sprint State"),
	Run_State UMETA(DisplayName = "Run State"),
	Crouch_State UMETA(DisplayName = "Crouch State"),
	AimCrouch_State UMETA(DisplayName = "AimCrouch State")
};

UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	PistolType UMETA(DisplayName = "Pistol"),
	RifleType UMETA(DisplayName = "Rifle"),
	ShotgunType UMETA(DisplayName = "Shotgun"),
	GrenadeLauncherType UMETA(DisplayName = "GrenadeLauncher")
};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	float AimSpeed = 300.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	float SprintSpeed = 800.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	float RunSpeed = 600.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	float CrouchSpeed = 300.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	float AimCrouchSpeed = 300.0f;
};

USTRUCT(BlueprintType)
struct FProjectileInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	TSubclassOf<class AProjectileDefault> Projectile = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	UParticleSystem* TraceFX = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	FTransform TraceFXOffset = FTransform();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	float ProjectileDamage = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	float ProjectileLifeTime = 10.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	float ProjectileInitSpeed = 2000.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GrenadeProjectile")
	UStaticMesh* ProjectileStaticMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GrenadeProjectile")
	FTransform ProjectileStaticMeshOffset = FTransform();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hit Surfase Effect")
	TMap<TEnumAsByte<EPhysicalSurface>, UMaterialInterface*> HitDecal;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hit Surfase Effect")
	TMap<TEnumAsByte<EPhysicalSurface>, USoundBase*> HitSound;
	//USoundBase* HitSound = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hit Surfase Effect")
	TMap<TEnumAsByte<EPhysicalSurface>, UParticleSystem*> HitFX;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explosion")
	UParticleSystem* ExplosionFX = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explosion")
	USoundBase* ExplosionSound = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explosion")
	float ExplosionMaxRadiusDamage = 200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explosion")
	float ExplosionMinRadiusDamage = 100.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explosion")
	float ExplosionMaxDamage = 40.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explosion")
	float ExplosionFalloffCoefficient = 1.0f;
};

USTRUCT(BlueprintType)
struct FWeaponDispersion
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AimSate Dispersion")
	float AimState_DispersionMax = 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AimSate Dispersion")
	float AimState_DispersionMin = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AimSate Dispersion")
	float AimState_Recoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AimSate Dispersion")
	float AimState_Reduction = 0.1f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AimCrouch Dispersion")
	float AimCrouch_DispersionMax = 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AimCrouch Dispersion")
	float AimCrouch_DispersionMin = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AimCrouch Dispersion")
	float AimCrouch_Recoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AimCrouch Dispersion")
	float AimCrouch_Reduction = 0.1f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Crouch Dispersion")
	float Crouch_DispersionMax = 3.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Crouch Dispersion")
	float Crouch_DispersionMin = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Crouch Dispersion")
	float Crouch_Recoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Crouch Dispersion")
	float Crouch_Reduction = 0.1f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RunState Dispersion")
	float Run_DispersionMax = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RunState Dispersion")
	float Run_DispersionMin = 3.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RunState Dispersion")
	float Run_Recoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RunState Dispersion")
	float Run_Reduction = 0.1f;
};

USTRUCT(BlueprintType)
struct FAnimationWeaponInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character Animation")
	UAnimMontage* FireMontage = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character Animation")
	UAnimMontage* AimFireMontage = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character Animation")
	UAnimMontage* ReloadingMontage = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character Animation")
	UAnimMontage* AimReloadingMontage = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Animation")
	UAnimMontage* WeaponReloadingMontage = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Animation")
	UAnimMontage* WeaponAimReloadingMontage = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Animation")
	UAnimMontage* WeaponFireMontage = nullptr;
};

USTRUCT(BlueprintType)
struct FDropMeshInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Drop Mesh")
	UStaticMesh* DropMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Drop Mesh")
	float DropMeshTime = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Drop Mesh")
	float DropMeshLifeTime = 3.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Drop Mesh")
	FTransform DropMeshOffset = FTransform();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Drop Mesh")
	FVector DropImpulseVector = FVector(0.0f);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Drop Mesh")
	float PowerImpulse = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Drop Mesh")
	float RandomImpulseDispersion = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Drop Mesh")
	float CustomMass = 0.0f;
};

USTRUCT(BlueprintType)
struct FWeaponInfo : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Class")
	TSubclassOf<class AWeaponDefault> WeaponClass = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory ")
	float SwitchTimeToWeapon = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory ")
	UTexture2D* WeaponIcon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory ")
	EWeaponType WeaponType = EWeaponType::RifleType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Defaults")
	float RateOfFire = 0.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Defaults")
	float ReloadTime = 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Defaults")
	int32 MaxRound = 10;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Defaults")
	int32 NumberProjectileByShot = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	FWeaponDispersion WeaponDispersion;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
	FProjectileInfo ProjectileSettings;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitScanSettings")
	float WeaponDamage = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitScanSettings")
	float TraceDistance = 2000.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitScanSettings")
	USoundBase* SoundFireWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitScanSettings")
	USoundBase* SoundReloadWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitScanSettings")
	UParticleSystem* EffectFireWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitScanSettings")
	UDecalComponent* DecalOnHit = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
	FAnimationWeaponInfo WeaponAnimationInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Drop Mesh")
	FDropMeshInfo DropClip;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Drop Mesh")
	FDropMeshInfo DropShells;
};

USTRUCT(BlueprintType)
struct FAdditionalWeaponInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Stats")
	int32 Round = 999;
};

USTRUCT(BlueprintType)
struct FWeaponSlot
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSlot")
	FName NameItem;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSlot")
	FAdditionalWeaponInfo AdditionalInfo;
};

USTRUCT(BlueprintType)
struct FAmmoSlot
{
	GENERATED_BODY()

	///Index Slot by Index Array
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
	EWeaponType WeaponType = EWeaponType::RifleType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
	int32 Count = 100;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
	int32 MaxCount = 100;
};

USTRUCT(BlueprintType)
struct FDropItem : public FTableRowBase
{
	GENERATED_BODY()

	///Index Slot by Index Array
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
	UStaticMesh* WeaponStaticMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
	USkeletalMesh* WeaponSkeletalMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
	FWeaponSlot WeaponInfo;
};

UCLASS()
class TDSPROJECT_API UTypes: public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
};
