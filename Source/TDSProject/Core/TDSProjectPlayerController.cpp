// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSProjectPlayerController.h"
#include "Engine/World.h"

ATDSProjectPlayerController::ATDSProjectPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}

void ATDSProjectPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);
	
}

void ATDSProjectPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();
	
}
