// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TDSProjectPlayerController.generated.h"

UCLASS()
class ATDSProjectPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ATDSProjectPlayerController();

protected:

	// Begin PlayerController interface
	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	// End PlayerController interface
	
};


