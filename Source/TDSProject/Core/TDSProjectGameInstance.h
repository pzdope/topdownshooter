﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Engine/DataTable.h"
#include "TDSProject/FunctionLibrary/Types.h"
#include "TDSProjectGameInstance.generated.h"

UCLASS()
class TDSPROJECT_API UTDSProjectGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
 
public:
	//Table
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSettings")
	UDataTable* WeaponInfoTable = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = " WeaponSetting ")
	UDataTable* DropItemInfoTable = nullptr;
	
	UFUNCTION(BlueprintCallable)
	bool GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo);
	UFUNCTION(BlueprintCallable)
	bool GetDropItemInfoByName(FName NameItem, FDropItem& OutInfo);
};
