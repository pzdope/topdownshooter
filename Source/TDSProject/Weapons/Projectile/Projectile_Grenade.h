// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDSProject/Weapons/Projectile/ProjectileDefault.h"
#include "Projectile_Grenade.generated.h"

UCLASS()
class TDSPROJECT_API AProjectile_Grenade : public AProjectileDefault
{
	GENERATED_BODY()
protected:
	virtual void BeginPlay() override;
public:
	virtual void Tick(float DeltaTime) override;
	
	void TimerExplose(float DeltaTime);
	virtual void BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
		FVector NormalImpulse, const FHitResult& Hit) override;
	virtual void ImpactProjectile() override;
	void Explose();

	bool TimerEnabled = false;
	float TimerToExplose = 0.0f;
	float TimeToExplose = 2.0f;
};
