// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSProject/Weapons/Projectile/Projectile_Grenade.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"

int32 DebugExlodeShow = 0;
FAutoConsoleVariable CVARExplodeShow(
	TEXT("TDSProject.DebugExplode"),
	DebugExlodeShow,
	TEXT("DrawDebug for explode"),
	ECVF_Cheat);

void AProjectile_Grenade::BeginPlay()
{
	Super::BeginPlay();
}

void AProjectile_Grenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimerExplose(DeltaTime);
}

void AProjectile_Grenade::TimerExplose(float DeltaTime)
{
	if(TimerEnabled)
	{
		if(TimerToExplose > TimeToExplose)
		{
			Explose();
		}
		else
		{
			TimerToExplose += DeltaTime;
		}
	}
}

void AProjectile_Grenade::BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectile_Grenade::ImpactProjectile()
{
	TimerEnabled = true;
}

void AProjectile_Grenade::Explose()
{
	DebugExlodeShow = 1;
	if(DebugExlodeShow)
	{
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSettings.ExplosionMinRadiusDamage, 24, FColor::Red, false, 5.0f);
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSettings.ExplosionMaxRadiusDamage, 24, FColor::Yellow, false, 4.0f);
		DrawDebugSphere(GetWorld(), GetActorLocation(), 300.0f, 24, FColor::Green, false, 3.0f);
	}
	
	TimerEnabled = false;
	if(ProjectileSettings.ExplosionFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSettings.ExplosionFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
	}
	if(ProjectileSettings.ExplosionSound)
	{
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), ProjectileSettings.ExplosionSound, GetActorLocation());
	}

	TArray<AActor*> IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(), ProjectileSettings.ExplosionMaxDamage,
		ProjectileSettings.ExplosionMaxDamage*0.2f, GetActorLocation(), 1000.0f, 2000.0f,5,
		NULL, IgnoredActor, this, nullptr);
	this->Destroy();
}
