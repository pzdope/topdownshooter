// Fill out your copyright notice in the Description page of Project Settings.

#include "WeaponDefault.h"
#include "DrawDebugHelpers.h"
#include "Engine/StaticMeshActor.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Projectile/ProjectileDefault.h"
#include "TDSProject/Character/InventoryComponent.h"

// Sets default values
AWeaponDefault::AWeaponDefault()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("No Collision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("No Collision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootVector = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootVector->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();

	WeaponInit();
}

// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FireTick(DeltaTime);
	ReloadTick(DeltaTime);
	DispersionTick(DeltaTime);
	ShellDropTick(DeltaTime);
	ClipDropTick(DeltaTime);
}

void AWeaponDefault::FireTick(float DeltaTime)
{
	if (WeaponFiring && GetWeaponRound() > 0 && !WeaponReloading)
	{
		if (FireTimer < 0.0f)
		{
			Fire();
		}
		else
		{
			FireTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::ReloadTick(float DeltaTime)
{
	if (WeaponReloading)
	{
		if (ReloadTimer < 0.0f)
		{
			FinishReload();
		}
		else
		{
			ReloadTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::DispersionTick(float DeltaTime)
{
	if (!WeaponReloading)
	{
		if (!WeaponFiring)
		{
			if (bShouldReduceDispersion)
			{
				CurrentDispersion -= CurrentDispersionReduction;
			}
			else
			{
				CurrentDispersion += CurrentDispersionReduction;
			}
		}
		if (CurrentDispersion < CurrentDispersionMin)
		{
			CurrentDispersion = CurrentDispersionMin;
		}
		else
		{
			if (CurrentDispersion > CurrentDispersionMax)
			{
				CurrentDispersion = CurrentDispersionMax;
			}
		}
	}
	/*if(ShowDebug)
		UE_LOG(LogTemp, Warning, TEXT("Dispersion: MAX = %f. MIN = %f. Current = %f"), CurrentDispersionMax,
		       CurrentDispersionMin, CurrentDispersion);*/
}

void AWeaponDefault::ClipDropTick(float DeltaTime)
{
	if (DropClipFlag)
	{
		if (DropClipTimer < 0.0f)
		{
			DropClipFlag = false;
			InitDropMesh(WeaponSettings.DropClip.DropMesh, WeaponSettings.DropClip.DropMeshOffset,
			             WeaponSettings.DropClip.DropImpulseVector, WeaponSettings.DropClip.DropMeshLifeTime,
			             WeaponSettings.DropClip.RandomImpulseDispersion, WeaponSettings.DropClip.PowerImpulse,
			             WeaponSettings.DropClip.CustomMass);
		}
		else
		{
			DropClipTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::ShellDropTick(float DeltaTime)
{
	if (DropShellFlag)
	{
		if (DropShellTimer < 0.0f)
		{
			DropShellFlag = false;
			InitDropMesh(WeaponSettings.DropShells.DropMesh, WeaponSettings.DropShells.DropMeshOffset,
			             WeaponSettings.DropShells.DropImpulseVector, WeaponSettings.DropShells.DropMeshLifeTime,
			             WeaponSettings.DropShells.RandomImpulseDispersion, WeaponSettings.DropShells.PowerImpulse,
			             WeaponSettings.DropShells.CustomMass);
		}
		else
		{
			DropShellTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::WeaponInit()
{
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
	{
		SkeletalMeshWeapon->DestroyComponent(true);
	}
	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
	{
		StaticMeshWeapon->DestroyComponent();
	}

	UpdateStateWeapon(EMovementState::Run_State);
}

void AWeaponDefault::SetWeaponStateFire(bool bIsFire)
{
	if (CheckWeaponCanFire() && bIsFire) // && bIsFire
	{
		WeaponFiring = bIsFire;
	}
	else
	{
		WeaponFiring = false;
		FireTimer = 0.01f;
	}
}

bool AWeaponDefault::CheckWeaponCanFire() const
{
	return !BlockFire;
}

FProjectileInfo AWeaponDefault::GetProjectile()
{
	return WeaponSettings.ProjectileSettings;
}

void AWeaponDefault::Fire()
{
	UAnimMontage* AnimToPlay = nullptr;
	if (WeaponAiming)
	{
		AnimToPlay = WeaponSettings.WeaponAnimationInfo.AimFireMontage;
	}
	else
	{
		AnimToPlay = WeaponSettings.WeaponAnimationInfo.FireMontage;
	}

	if (WeaponSettings.WeaponAnimationInfo.WeaponFireMontage
		&& SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
	{
		SkeletalMeshWeapon->GetAnimInstance()->Montage_Play(WeaponSettings.WeaponAnimationInfo.WeaponFireMontage);
	}

	if (WeaponSettings.DropShells.DropMesh)
	{
		if (WeaponSettings.DropShells.DropMeshTime < 0.0f)
		{
			InitDropMesh(WeaponSettings.DropShells.DropMesh, WeaponSettings.DropShells.DropMeshOffset,
			             WeaponSettings.DropShells.DropImpulseVector, WeaponSettings.DropShells.DropMeshLifeTime,
			             WeaponSettings.DropShells.RandomImpulseDispersion, WeaponSettings.DropShells.PowerImpulse,
			             WeaponSettings.DropShells.CustomMass);
		}
		else
		{
			DropShellFlag = true;
			DropShellTimer = WeaponSettings.DropShells.DropMeshTime;
		}
	}

	FireTimer = WeaponSettings.RateOfFire;
	AdditionalWeaponInfo.Round = AdditionalWeaponInfo.Round - 1;
	ChangeDispersionByShoot();

	OnWeaponFireStart.Broadcast(AnimToPlay);

	UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSettings.SoundFireWeapon,
	                                       ShootVector->GetComponentLocation());
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponSettings.EffectFireWeapon,
	                                         ShootVector->GetComponentTransform());

	int8 NumberProjectile = GetNumberProjectileByShot();

	if (ShootVector)
	{
		FVector SpawnLocation = ShootVector->GetComponentLocation();
		FRotator SpawnRotation = ShootVector->GetComponentRotation();
		FProjectileInfo ProjectileInfo;
		ProjectileInfo = GetProjectile();

		FVector EndLocation;
		for (int8 i = 0; i < NumberProjectile; i++) // Shotgun
		{
			EndLocation = GetFireEndLocation();

			if (ProjectileInfo.Projectile)
			{
				// Projectile Fire
				FVector Dir = EndLocation - SpawnLocation;

				Dir.Normalize();

				FMatrix myMatrix(Dir, FVector(0, 0, 0), FVector(0, 0, 0), FVector::ZeroVector);
				SpawnRotation = myMatrix.Rotator();

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AProjectileDefault* myProjectile = Cast<AProjectileDefault>(
					GetWorld()->SpawnActor(ProjectileInfo.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myProjectile)
				{
					myProjectile->InitProjectile(WeaponSettings.ProjectileSettings);
				}
			}
			else
			{
				// Trace Fire
				FHitResult Hit;
				TArray<AActor*> Actors;

				//ShowDebug = true;
				EDrawDebugTrace::Type DebugTrace;
				if (ShowDebug)
				{
					DrawDebugLine(GetWorld(), SpawnLocation,
					              SpawnLocation + ShootVector->GetForwardVector() * WeaponSettings.TraceDistance,
					              FColor::Yellow, false, 3.0f, (uint8)'\000', 0.5f);
					DebugTrace = EDrawDebugTrace::ForDuration;
				}
				else
				{
					DebugTrace = EDrawDebugTrace::None;
				}

				UKismetSystemLibrary::LineTraceSingle(GetWorld(), SpawnLocation,
				                                      EndLocation * WeaponSettings.TraceDistance,
				                                      ETraceTypeQuery::TraceTypeQuery4, false, Actors, DebugTrace, Hit,
				                                      true, FLinearColor::Red,
				                                      FLinearColor::Green, 5.0f);

				if (Hit.GetActor() && Hit.PhysMaterial.IsValid())
				{
					EPhysicalSurface mySurfaceType = UGameplayStatics::GetSurfaceType(Hit);

					if (WeaponSettings.ProjectileSettings.HitDecal.Contains(mySurfaceType))
					{
						UMaterialInterface* myMaterial = WeaponSettings.ProjectileSettings.HitDecal[mySurfaceType];

						if (myMaterial && Hit.GetComponent())
						{
							UGameplayStatics::SpawnDecalAttached(myMaterial, FVector(10.0f), Hit.GetComponent(),
							                                     NAME_None, Hit.ImpactPoint,
							                                     Hit.ImpactNormal.Rotation(),
							                                     EAttachLocation::KeepWorldPosition, 10.0f);
						}
					}
					if (WeaponSettings.ProjectileSettings.HitFX.Contains(mySurfaceType))
					{
						UParticleSystem* myParticle = WeaponSettings.ProjectileSettings.HitFX[mySurfaceType];
						if (myParticle)
						{
							UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), myParticle,
							                                         FTransform(Hit.ImpactNormal.Rotation(),
								                                         Hit.ImpactPoint, FVector(1.0f)));
						}
					}

					if (WeaponSettings.ProjectileSettings.HitSound.Contains(mySurfaceType))
					{
						UGameplayStatics::PlaySoundAtLocation(GetWorld(), WeaponSettings.ProjectileSettings.HitSound[mySurfaceType],
						                                      Hit.ImpactPoint);
					}

					UGameplayStatics::ApplyPointDamage(Hit.GetActor(),
					                                   WeaponSettings.ProjectileSettings.ProjectileDamage,
					                                   Hit.TraceStart, Hit, GetInstigatorController(), this,NULL);
				}
			}
		}
	}
	/*if (GetWeaponRound() <= 0 && !WeaponReloading)
	{
		//Init Reload
		if(CheckWeaponCanReload())
			InitReload();
	}*/
}

void AWeaponDefault::UpdateStateWeapon(EMovementState NewMovementState)
{
	BlockFire = false;

	switch (NewMovementState)
	{
	case EMovementState::Aim_State:
		WeaponAiming = true;
		CurrentDispersionMax = WeaponSettings.WeaponDispersion.AimState_DispersionMax;
		CurrentDispersionMin = WeaponSettings.WeaponDispersion.AimState_DispersionMin;
		CurrentDispersionRecoil = WeaponSettings.WeaponDispersion.AimState_Recoil;
		CurrentDispersionReduction = WeaponSettings.WeaponDispersion.AimState_Reduction;
		break;
	case EMovementState::AimCrouch_State:
		WeaponAiming = true;
		CurrentDispersionMax = WeaponSettings.WeaponDispersion.AimCrouch_DispersionMax;
		CurrentDispersionMin = WeaponSettings.WeaponDispersion.AimCrouch_DispersionMin;
		CurrentDispersionRecoil = WeaponSettings.WeaponDispersion.AimCrouch_Recoil;
		CurrentDispersionReduction = WeaponSettings.WeaponDispersion.AimCrouch_Reduction;
		break;
	case EMovementState::Crouch_State:
		WeaponAiming = false;
		CurrentDispersionMax = WeaponSettings.WeaponDispersion.Crouch_DispersionMax;
		CurrentDispersionMin = WeaponSettings.WeaponDispersion.Crouch_DispersionMin;
		CurrentDispersionRecoil = WeaponSettings.WeaponDispersion.Crouch_Recoil;
		CurrentDispersionReduction = WeaponSettings.WeaponDispersion.AimState_Reduction;
		break;
	case EMovementState::Run_State:
		WeaponAiming = false;
		CurrentDispersionMax = WeaponSettings.WeaponDispersion.Run_DispersionMax;
		CurrentDispersionMin = WeaponSettings.WeaponDispersion.Run_DispersionMin;
		CurrentDispersionRecoil = WeaponSettings.WeaponDispersion.Run_Recoil;
		CurrentDispersionReduction = WeaponSettings.WeaponDispersion.AimState_Reduction;
		break;
	case EMovementState::Sprint_State:
		WeaponAiming = false;
		BlockFire = true;
		SetWeaponStateFire(false);
		break;
	default:
		break;
	}
}

void AWeaponDefault::ChangeDispersionByShoot()
{
	CurrentDispersion += CurrentDispersionRecoil;
}

float AWeaponDefault::GetCurrentDispersion() const
{
	const float Result = CurrentDispersion;
	return Result;
}

FVector AWeaponDefault::ApplyDispersionToShoot(FVector DirectionShoot) const
{
	return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.0f);
}

FVector AWeaponDefault::GetFireEndLocation() const
{
	FVector EndLocation;

	FVector tmpV = (ShootVector->GetComponentLocation() - ShootEndLocation);

	if (tmpV.Size() > SizeVectorToChangeShootDirectionLogic)
	{
		EndLocation = ShootVector->GetComponentLocation() + ApplyDispersionToShoot((ShootVector->GetComponentLocation()
			- ShootEndLocation).GetSafeNormal()) * -100000.0f;
		if (ShowDebug)
			DrawDebugCone(GetWorld(), ShootVector->GetComponentLocation(), -(ShootVector->GetComponentLocation()
				              - ShootEndLocation), WeaponSettings.TraceDistance, GetCurrentDispersion() * PI / 180.0f,
			              GetCurrentDispersion() * PI / 180.0f, 32, FColor::Emerald, false, 1.0f,
			              (uint8)'\000', 1.0f);
	}
	else
	{
		EndLocation = ShootVector->GetComponentLocation() + ApplyDispersionToShoot(ShootVector->GetForwardVector()) *
			100000.0f;
		if (ShowDebug)
			DrawDebugCone(GetWorld(), ShootVector->GetComponentLocation(), ShootVector->GetForwardVector(),
			              WeaponSettings.TraceDistance, GetCurrentDispersion() * PI / 180.0f,
			              GetCurrentDispersion() * PI / 180.0f, 32, FColor::Emerald, false, 0.1f, (uint8)'\000', 1.0f);
	}

	if (ShowDebug)
	{
		//Direction weapon look
		DrawDebugLine(GetWorld(), ShootVector->GetComponentLocation(),
		              ShootVector->GetComponentLocation() + ShootVector->GetForwardVector() * 500.0f, FColor::Cyan,
		              false, 5.0f, (uint8)'\000', 0.5f);
		//Direction projectile must fly
		DrawDebugLine(GetWorld(), ShootVector->GetComponentLocation(), ShootEndLocation, FColor::Red, false, 5.0f,
		              (uint8)'\000', 0.5f);
		//Direction projectile current fly
		DrawDebugLine(GetWorld(), ShootVector->GetComponentLocation(), EndLocation, FColor::Black, false, 5.0f,
		              (uint8)'\000', 0.5f);
	}
	return EndLocation;
}

int8 AWeaponDefault::GetNumberProjectileByShot() const
{
	return WeaponSettings.NumberProjectileByShot;
}

int32 AWeaponDefault::GetWeaponRound()
{
	return AdditionalWeaponInfo.Round;
}

void AWeaponDefault::InitReload()
{
	WeaponReloading = true;

	ReloadTimer = WeaponSettings.ReloadTime;

	UAnimMontage* AnimToPlay;
	if (WeaponAiming)
	{
		AnimToPlay = WeaponSettings.WeaponAnimationInfo.AimReloadingMontage;
	}
	else
	{
		AnimToPlay = WeaponSettings.WeaponAnimationInfo.ReloadingMontage;
	}

	UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSettings.SoundReloadWeapon, GetActorLocation());

	OnWeaponReloadStart.Broadcast(AnimToPlay);

	UAnimMontage* AnimWeaponToPlay;
	if (WeaponAiming)
	{
		AnimWeaponToPlay = WeaponSettings.WeaponAnimationInfo.WeaponAimReloadingMontage;
	}
	else
	{
		AnimWeaponToPlay = WeaponSettings.WeaponAnimationInfo.WeaponReloadingMontage;
	}

	if (WeaponSettings.WeaponAnimationInfo.WeaponReloadingMontage
		&& SkeletalMeshWeapon
		&& SkeletalMeshWeapon->GetAnimInstance())
	{
		SkeletalMeshWeapon->GetAnimInstance()->Montage_Play(AnimWeaponToPlay);
	}

	if (WeaponSettings.DropClip.DropMesh)
	{
		DropClipFlag = true;
		DropClipTimer = WeaponSettings.DropClip.DropMeshTime;
	}
}

void AWeaponDefault::FinishReload()
{
	int8 AvailableAmmoFromInventory = GetAvailableAmmoForReload();
	int8 AmmoNeedTakeFromInv;
	int8 NeedToReload = WeaponSettings.MaxRound - AdditionalWeaponInfo.Round;

	if (NeedToReload > AvailableAmmoFromInventory)
	{
		AdditionalWeaponInfo.Round = AvailableAmmoFromInventory;
		AmmoNeedTakeFromInv = AvailableAmmoFromInventory;
	}
	else
	{
		AdditionalWeaponInfo.Round += NeedToReload;
		AmmoNeedTakeFromInv = NeedToReload;
	}

	OnWeaponReloadEnd.Broadcast(true, -AmmoNeedTakeFromInv);
}

void AWeaponDefault::CancelReload()
{
	WeaponReloading = false;
	if (SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
		SkeletalMeshWeapon->GetAnimInstance()->StopAllMontages(0.15f);

	OnWeaponReloadEnd.Broadcast(false, 0);
	DropClipFlag = false;
}

bool AWeaponDefault::CheckCanWeaponReload()
{
	bool result = true;
	if (GetOwner())
	{
		UInventoryComponent* MyInv = Cast<UInventoryComponent>(
			GetOwner()->GetComponentByClass(UInventoryComponent::StaticClass()));
		if (MyInv)
		{
			int8 AvailableAmmoForWeapon;
			if (!MyInv->CheckAmmoForWeapon(WeaponSettings.WeaponType, AvailableAmmoForWeapon))
			{
				result = false;
			}
		}
	}

	return result;
}

int8 AWeaponDefault::GetAvailableAmmoForReload()
{
	int8 AvailableAmmoForWeapon = WeaponSettings.MaxRound;
	if (GetOwner())
	{
		UInventoryComponent* MyInv = Cast<UInventoryComponent>(
			GetOwner()->GetComponentByClass(UInventoryComponent::StaticClass()));
		if (MyInv)
		{
			if (MyInv->CheckAmmoForWeapon(WeaponSettings.WeaponType, AvailableAmmoForWeapon))
			{
				//AvailableAmmoForWeapon = AvailableAmmoForWeapon;
			}
		}
	}
	return AvailableAmmoForWeapon;
}

void AWeaponDefault::InitDropMesh(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection,
                                  float LifeTimeMesh, float ImpulseRandomDispersion, float PowerImpulse,
                                  float CustomMass)
{
	if (DropMesh)
	{
		FTransform Transform;

		FVector LocalDir = this->GetActorForwardVector() * Offset.GetLocation().X + this->GetActorRightVector() * Offset
			.GetLocation().Y + this->GetActorUpVector() * Offset.GetLocation().Z;

		Transform.SetLocation(GetActorLocation() + LocalDir);
		Transform.SetScale3D(Offset.GetScale3D());

		Transform.SetRotation((GetActorRotation() + Offset.Rotator()).Quaternion());
		AStaticMeshActor* NewActor = nullptr;
		
		FActorSpawnParameters Param;
		Param.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
		Param.Owner = this;
		NewActor = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), Transform, Param);

		if (NewActor && NewActor->GetStaticMeshComponent())
		{
			NewActor->GetStaticMeshComponent()->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
			NewActor->GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);

			//Set parameter for new actor
			NewActor->SetActorTickEnabled(false);
			NewActor->SetLifeSpan(LifeTimeMesh);

			NewActor->GetStaticMeshComponent()->Mobility = EComponentMobility::Movable;
			NewActor->GetStaticMeshComponent()->SetSimulatePhysics(true);
			NewActor->GetStaticMeshComponent()->SetStaticMesh(DropMesh);

			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(
				ECC_GameTraceChannel1, ECollisionResponse::ECR_Ignore);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(
				ECC_GameTraceChannel2, ECollisionResponse::ECR_Ignore);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Ignore);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(
				ECC_WorldStatic, ECollisionResponse::ECR_Block);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(
				ECC_WorldDynamic, ECollisionResponse::ECR_Block);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(
				ECC_PhysicsBody, ECollisionResponse::ECR_Block);

			if (CustomMass > 0.0f)
			{
				NewActor->GetStaticMeshComponent()->SetMassOverrideInKg(NAME_None, CustomMass, true);
			}

			if (!DropImpulseDirection.IsNearlyZero())
			{
				FVector FinalDir;
				LocalDir = LocalDir + (DropImpulseDirection * 1000.0f);

				if (!FMath::IsNearlyZero(ImpulseRandomDispersion))
					FinalDir += UKismetMathLibrary::RandomUnitVectorInConeInDegrees(LocalDir, ImpulseRandomDispersion);

				NewActor->GetStaticMeshComponent()->AddImpulse(FinalDir * PowerImpulse);
			}
		}
	}
}

